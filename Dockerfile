FROM nginx:1.23-alpine

RUN apk update && apk add --no-cache openssh rsync shadow

COPY default.conf /etc/nginx/conf.d/

# https://stackoverflow.com/questions/61833713/how-to-login-by-ssh-in-alpine-linux-without-passwords
# https://superuser.com/questions/1633085/dockerfile-clone-repo-with-passwordless-private-key-errors-authentication-ag
RUN echo -n "root:dummy_passwd" | chpasswd
RUN echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config

RUN mkdir /root/.ssh
COPY ./id_rsa.pub /root/.ssh/
RUN cat /root/.ssh/id_rsa.pub > /root/.ssh/authorized_keys
RUN chmod 700 /root/.ssh && chmod 600 /root/.ssh/*

ADD buildIndex.sh /root/buildIndex.sh
ADD crontab.txt /crontab.txt
RUN chmod +x /root/buildIndex.sh

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

EXPOSE 22 80

ENTRYPOINT ["/entrypoint.sh"]
