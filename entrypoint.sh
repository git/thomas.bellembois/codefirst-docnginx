#!/usr/bin/env sh

ssh-keygen -A

echo "Starting sshd:"
/usr/sbin/sshd -e "$@"

echo "Running crontab."
/usr/bin/crontab /crontab.txt &
/usr/sbin/crond -f -l 2 &

echo "Starting nginx in non daemon mode:"
chown -R nginx:nginx /usr/share/nginx
/usr/sbin/nginx -g 'daemon off;'