#!/usr/bin/env sh
INPUT=/usr/share/nginx/html/
OUTPUT=/usr/share/nginx/html/index.html

HTML_TOP=$(cat << EOF
<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>Code#0 documentation</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<style>
    body {
        margin: 0px;
        font-family: sans-serif;
    }
</style>
<body>
<i>Page updated every 5 minutes.</i>
<ul>
EOF
)

HTML_BOTTOM=$(cat << EOF
</ul>
</body>
</html>
EOF
)

echo $HTML_TOP > $OUTPUT

cd $INPUT
for d in */ ; do
    echo "<li><a target=\"_blank\" href=\"$d\">"$d"</a></li>" >> $OUTPUT
done

echo $HTML_BOTTOM >> $OUTPUT